#include <stdio.h>
#include "library.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

void app_main(void)
{
    struct data_collect gps_data;
    const char* gps_data_string = "$GPGGA,002153.000,3342.6618,N,11751.3858,W,1,10,1.2,27.0,M,-34.2,M,,0000*5E";

    if (parse_gps_data(&gps_data, gps_data_string)) {
        print_gps_data(&gps_data);
    } else {
        printf("Invalid GPS data\n");
    }
}


