struct gps_time {
    int hours;
    int minutes;
    int seconds;
    int milliseconds;
};

struct location {
    int deg;
    double min;
    char hem;
};

struct dist {
    float distance;
    char unit;
};

struct data_collect {
    struct gps_time time;
    struct location latitude;
    struct location longitude;
    int fix;
    int satellites;
    struct dist altitude;
    struct dist geoid;
    int dgps;
};

int checksum(const char* gps_data);
bool val_checksum(const char* gps_data);
bool parse_gps_data(struct data_collect* store, const char* gps_data);
void print_gps_data(struct data_collect* store);


    // $GPGGA,002153.000,3342.6618,N,11751.3858,W,1,10,1.2,27.0,M,-34.2,M,,0000*5E
    // packet definer: indicated the packet is of type GGA, time: HHMMSS.SSS,
    // latitude of GPS location: DDMM.MMMM, N: northern hemisphere,
    // longitude of GPC location: DDMM.MMMM, W: western hemisphere,
    // GPS fix: 1 means the GPS is giving accurate data,
    // 10: # of satellites tracked
    // 1.2: HDOP-horizontal dilution of precision
    // 27.0, M: altitude above mean sea level in meters
    // -34.2, M: height of geoid in meters
    // 0000: DGPS - differential GPS
    // *5E: checksum
