#include <stdio.h>
#include "library.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

int checksum(const char* gps_data) {
    int cs = 0;
    const char* p = gps_data;
    while (*p != '\0' && *p != '*') {
        cs ^= *p++;
    }
    return cs;
}

bool val_checksum(const char* gps_data) {
    bool result;
    const char* checkpoint = strchr(gps_data, '*');
    if (checkpoint == NULL) {
        return false;           // Invalid GPS data format
    }

    int GPSchecksum;
    sscanf(checkpoint + 1, "%2X", &GPSchecksum);
    int calculated_checksum = checksum(gps_data + 1);
    if (GPSchecksum == calculated_checksum)
    {result = true;}
    else {result = false;}
    return result;
}

bool parse_gps_data(struct data_collect* store, const char* gps_data) {
    if (!val_checksum(gps_data)) {
        return false; // Invalid checksum
    }

    char* data = strdup(gps_data);
    if (data == NULL) {
        return false; // Memory allocation error
    }

    char* token;
    int count = 1;

    token = strtok(data, ",");      //to store data from string gps_data seperated by a comma
    while (token != NULL) {
        switch (count) {
            case 2: // time: HHMMSS.SSS
                sscanf(token, "%02d%02d%02d.%03d", &store->time.hours, &store->time.minutes, &store->time.seconds, &store->time.milliseconds);
                break;
            case 3: // latitude of GPS location: DDMM.MMMM
                sscanf(token, "%02d%lf", &store->latitude.deg, &store->latitude.min);
                break;
            case 4: // hemisphere
                store->latitude.hem = token[0];
                break;
            case 5: // longitude of GPC location: DDMM.MMMM
                sscanf(token, "%03d%lf", &store->longitude.deg, &store->longitude.min);
                break;
            case 6: // W: western hemisphere,
                store->longitude.hem = token[0];
                break;
            case 7: // GPS fix
                store->fix = atoi(token);   //converting string to int and storing it in store->fix
                break;
            case 8: // # of satellites tracked
                store->satellites = atoi(token);
                break;
            case 9: // altitude above mean sea level
                sscanf(token, "%f", &store->altitude.distance);
                break;
            case 10: // height of geoid
                sscanf(token, "%f", &store->geoid.distance);
                break;
            case 11: // DGPS
                store->dgps = atoi(token);
                break;
        }
        count++;
        token = strtok(NULL, ",");
    }

    free(data);
    return true;
}

void print_gps_data(struct data_collect* store) {
    printf("Time: %02d:%02d:%02d.%03d\n", store->time.hours, store->time.minutes, store->time.seconds, store->time.milliseconds);
    printf("Latitude: %02d° %.4lf' %c\n", store->latitude.deg, store->latitude.min, store->latitude.hem);
    printf("Longitude: %03d° %.4lf' %c\n", store->longitude.deg, store->longitude.min, store->longitude.hem);
    printf("Fix: %d\n", store->fix);
    printf("Satellites: %d\n", store->satellites);
    printf("Altitude: %f %c\n", store->altitude.distance, store->altitude.unit);
    printf("Geoid: %f %c\n", store->geoid.distance, store->geoid.unit);
    printf("DGPS: %d\n", store->dgps);
}

