# GPS Task

## step 01: installing ESP-IDF
First of all, I started with installing the idf. I installed the espressif IDE. Once the installation of the software and the tools was complete, the next step was writing the code.

## step 02: Writing the code
### Understanding the packet GGA of NMEA
First I checked the whole format of the GGA packet. 
I then used the one given in the task, i.e., **$GPGGA,002153.000,3342.6618,N,11751.3858,W,1,10,1.2,27.0,M,-34.2,M,,0000*5E** 
and identified the different sets of data. It was as under;
-         packet definer: indicated the packet is of type GGA, time: HHMMSS.SSS, 
-         latitude of GPS location: DDMM.MMMM, N: northern hemisphere, 
-         longitude of GPC location: DDMM.MMMM, W: western hemisphere,
-         GPS fix: 1 means the GPS is giving accurate data,
-         10: # of satellites tracked,
-         1.2: HDOP-horizontal dilution of precision,
-         27.0, M: altitude above mean sea level in meters,
-         -34.2, M: height of geoid in meters,
-         0000: DGPS - differential GPS,
-         *5E: checksum

### Making the required structures to storing data
Once I knew what data was being provided by the GPS and it's format, the next step was making structures. 
It was important to ensure that the structures were able to store the data properly.
For time, I made the following structure, which could store hours, minutes, seconds and milliseconds

```
struct gps_time 
{
    int hours;
    int minutes;
    int seconds;
    int milliseconds;
};
```



Next, I made a structure called location. This had 3 variable of data types int, double and char for storind the degrees, minutes and hemispheres of the location.
This structure was for storing longitude and latitide information.

```
struct location 
{
    int deg;
    double min;
    char hem;
}; 
```


       
For the information related to the altitude and geoid, I made a structure called dist. This had a variable called distance of the float data type and a character called unit. distance stored the height and unit stored the unit.

struct dist 
{
    float distance;
    char unit;
};


Lastly, the structure that was to be returned by the _parse_gps_data_ function, was called _data_collect_. 
This structure had all the parameters with their specific structures as their types, e.g., for time, 'struct gps_time time', etc. 
For the constants, I only used simple integer type variables, such as for the number of satellites. 
```
struct data_collect 
{
    struct gps_time time;
    struct location latitude;
    struct location longitude;
    int fix;
    int satellites;
    struct dist altitude;
    struct dist geoid;
    int dgps;
};
```
    
### Declaring the required functions
Now the next step was making functions. In total, I made 4 functions. 2 were for checking the validity of the packet through checksum. 1 was for parsing the packet and returning a structure with all parameters, and the last one was for printing all the parameters.
This is what the declaration of the functions looked like;
```
int checksum(const char* gps_data);
bool val_checksum(const char* gps_data);
bool parse_gps_data(struct data_collect* store, const char* gps_data);
void print_gps_data(struct data_collect* store);
```

### Initializing the functions
#### checking validity of packet
##### **checksum**
The function _checksum_ takes the output of the GPS **$GPGGA,002153.000,3342.6618,N,11751.3858,W,1,10,1.2,27.0,M,-34.2,M,,0000*5E** and has a character pointer p. p points to the start of the output gps_data and performs XOR operation on the ASCII value of that character and the value of the integer cs. 
The while loop ensures that the pointer keeps performing the XOR operation and then moves on to the next character as long as the next character is not a * (after which the value of the checksum is present) or a \0 (a null character which shows the end of the string).
This function returns the calculated value of checksum as an integer. 
```
int checksum(const char* gps_data)  
{
    int cs = 0;
    const char* p = gps_data;
    while (*p != '\0' && *p != '*') 
    {
        cs ^= *p++;
    }
    return cs;
}
```


##### **val_checksum**
This function also takes the output of the GPS data as input. It has a character pointer called _checkpoint_ which checks the whole string until it finds the character '*'. It then points towards its address. 

Next, the conditional statement checks that the checkpoint is not NULL, i.e., it ensures that the '*' is found. If not found, then the function returns FALSE.

If found, the function extracts the 2 characters after the '*'. These 2 represent the value of the checksum in the GPS output. For this purpose, I have used the sscanf function, present in the _stdio.h_ library. Then if starts with the address next to the checkpoint, i.e., 'checkpoint+1' and then takes 2 hexadecimal characters which are specified by '%2X', and lastly it stores the value a GPSchecksum. 

Then this function calls the checksum function and a '+1' is used to skip the first character of the output of the GPS, as it is $ and is not included in the calculation of the checksum. This value is stored in the calculated_checksum integer. Lastly both the GPSchecksum and calculate_checksum are compared. If they are same, the function returns ture, else it returns false.


```
bool val_checksum(const char* gps_data) 
{
    bool result;
    const char* checkpoint = strchr(gps_data, '*');
    if (checkpoint == NULL) 
    {
        return false;           // Invalid GPS data format
    }
    int GPSchecksum;
    sscanf(checkpoint + 1, "%2X", &GPSchecksum);
    int calculated_checksum = checksum(gps_data + 1);
    if (GPSchecksum == calculated_checksum)
    {result = true;}
    else {result = false;}
    return result;
}
```

#### extracting data and storing as seperate parameters
##### **parse_gps_data**
The input arguments of this function are the output of the GPS and the structure 'store' of type data_collect. 

First, the function checks the validity of the GPS data by calling the function _val_checksum_. If the function returns false, then the checksum is invalid and the function returns false.

Next, the _strdup_ function creates a duplicate of the gps_data string by allocating dynamic memory. Next, conditional is used to check that memory is allocated correctly. If not, the function returns false.

Then the duplicated string is tokenized using the strtok function. The tokens are separated by commas since the GPS data is expected to be in a comma-separated format. Each token represents a specific piece of information from the GPS data.

Within a loop, the function examines each token and extracts the relevant data based on its position, which is checked using an integer 'count' within the comma-separated string. The sscanf function is used with the appropriate format, based on th format of the GGA packet, to store the values in the specific structures and variables. This is done for all the values i.e., time, latitude, longitude, GPS fix status, number of satellites, altitude, height of geoid, and DGPS information are parsed and stored in the store structure.

Lastly, free is used to delete the dynamically allocated memory to the duplicate of the gps_data. And then the function returns true to indicate successful parsing and storing of parameters of the data.


```
bool parse_gps_data(struct data_collect* store, const char* gps_data) 
{
    if (!val_checksum(gps_data)) 
    {
        return false; // Invalid checksum
    }

    char* data = strdup(gps_data);
    if (data == NULL) 
    {
        return false; // Memory allocation error
    }

    char* token;
    int count = 1;

    token = strtok(data, ",");    
    while (token != NULL) 
    {
        switch (count) 
        {
            case 2: // time: HHMMSS.SSS
                sscanf(token, "%02d%02d%02d.%03d", &store->time.hours, &store->time.minutes, &store->time.seconds, &store->time.milliseconds);
                break;
            case 3: // latitude of GPS location: DDMM.MMMM 
                sscanf(token, "%02d%lf", &store->latitude.deg, &store->latitude.min);
                break;
            case 4: // hemisphere
                store->latitude.hem = token[0];
                break;
            case 5: // longitude of GPC location: DDMM.MMMM
                sscanf(token, "%03d%lf", &store->longitude.deg, &store->longitude.min);
                break;
            case 6: // W: western hemisphere,
                store->longitude.hem = token[0];
                break;
            case 7: // GPS fix
                store->fix = atoi(token);   //converting string to int and storing it in store->fix
                break;
            case 8: // # of satellites tracked
                store->satellites = atoi(token);
                break;
            case 9: // altitude above mean sea level 
                sscanf(token, "%f", &store->altitude.distance);
                break;
            case 10: // height of geoid
                sscanf(token, "%f", &store->geoid.distance);
                break;
            case 11: // DGPS
                store->dgps = atoi(token);
                break;
        }
        count++;
        token = strtok(NULL, ",");
    }

    free(data);
    return true;
}
```

#### printing the parameters
##### **print_gps_data**

This function takes a pointer to the data_collect structure an input argument. 

It uses the printf function to print the GPS information in a specific format. The formats within the printf statements are used to display the values from the data_collect structure. The formats are specified based on the format of the GGA packet.
Each printf statement is responsible for displaying a specific piece of information, such as time, latitude, longitude, fix status, etc.

```
void print_gps_data(struct data_collect* store) 
{
    printf("Time: %02d:%02d:%02d.%03d\n", store->time.hours, store->time.minutes, store->time.seconds, store->time.milliseconds);
    printf("Latitude: %02d° %.4lf' %c\n", store->latitude.deg, store->latitude.min, store->latitude.hem);
    printf("Longitude: %03d° %.4lf' %c\n", store->longitude.deg, store->longitude.min, store->longitude.hem);
    printf("Fix: %d\n", store->fix);
    printf("Satellites: %d\n", store->satellites);
    printf("Altitude: %f %c\n", store->altitude.distance, store->altitude.unit);
    printf("Geoid: %f %c\n", store->geoid.distance, store->geoid.unit);
    printf("DGPS: %d\n", store->dgps);
}
```

### Checking that the code compiles
Once all the functions were written, I wrote a simple code in main and called the _parse_gps_data_ function to ensure that the code compiles without any error. 

I used the online C compiler https://www.onlinegdb.com/online_c_compiler

Once the code compiled successfully without any errors, it was time to move on to the next step.

## step 03: Making custom library
Since I had already installed esp-idf and also had the code, I just opened the IDE and made a new folder. I then copied my main into the main.c file.
Next I made a new component and named it 'library'. This created the library.c and library.h files.
I included the custom library in the main.c file.
`#include "library.h"`

### library.h - the header file
In the library.h file, I included all the definitions of the structures and declarations of the functions. 

### library.c file
In the library.c file, I placed all the initializatins of the functions. All the necessary libraries as well as the header file of the custom library was included in this file.

## step 04: testing and uploading on git
The 4th and final step was testing. The code compiled successfully so I made an account og GitLab and uploaded all my files there.




